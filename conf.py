#
# Configuration file for the Sphinx documentation builder.
# http://www.sphinx-doc.org/en/stable/config

import platform
from datetime import datetime
from zoneinfo import ZoneInfo

import sphinx
import sphinx_material

project = "Luttes contre blockchain"
html_title = project
html_logo = "_static/logo.png"
author = "humain·e"
now = datetime.now(tz=ZoneInfo("Europe/Paris"))
version = f"{now.year}-{now.month:02}-{now.day:02} {now.hour:02}H ({now.tzinfo})"

today=version

extensions = [
    "sphinx.ext.intersphinx",
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]
html_static_path = ["_static"]
html_show_sourcelink = True
html_sidebars = {
    "**": ["logo-text.html", "globaltoc.html", "localtoc.html", "searchbox.html"]
}
extensions.append("sphinx_material")
html_theme_path = sphinx_material.html_theme_path()
html_context = sphinx_material.get_html_context()
html_theme = "sphinx_material"

extensions += ["sphinx.ext.todo"]
todo_include_todos = True

# material theme options (see theme.conf for more information)
# https://gitlab.com/bashtage/sphinx-material/blob/master/sphinx_material/sphinx_material/theme.conf
# Colors
# The theme color for mobile browsers. Hex color.
# theme_color = #3f51b5
# Primary colors:
# red, pink, purple, deep-purple, indigo, blue, light-blue, cyan,
# teal, green, light-green, lime, yellow, amber, orange, deep-orange,
# brown, grey, blue-grey, white
# Accent colors:
# red, pink, purple, deep-purple, indigo, blue, light-blue, cyan,
# teal, green, light-green, lime, yellow, amber, orange, deep-orange
# color_accent = blue
# color_primary = blue-grey

html_theme_options = {
    "base_url": "https://luttes.frama.io/contre/blockchain/",
    "repo_url": "https://framagit.org/luttes/contre/blockchain",
    "repo_name": project,
    "html_minify": False,
    "html_prettify": True,
    "css_minify": True,
    "repo_type": "gitlab",
    "globaltoc_depth": -1,
    "color_primary": "black",
    "color_accent": "black",
    "theme_color": "#2196f3",
    "nav_title": f"{project} ({today})",
    "master_doc": False,
    "nav_links": [
        {
            "href": "genindex",
            "internal": True,
            "title": "Index",
        },
        {
            "href": "https://pablockchain.fr/",
            "internal": False,
            "title": "pablockchain",
        },
        {
            "href": "https://grenoble.frama.io/linkertree/",
            "internal": False,
            "title": "Liens Grenobleluttes",
        },
    ],
    "heroes": {
        "index": "Luttes contre blockchain",
    },
    "table_classes": ["plain"],
}
# https://github.com/sphinx-contrib/yasfb
extensions.append("yasfb")
feed_base_url = html_theme_options["base_url"]
feed_author = "Scribe"
# https://sphinx-design.readthedocs.io/en/furo-theme/get_started.html
extensions.append("sphinx_design")
# https://sphinx-tags.readthedocs.io/en/latest/quickstart.html#installation
extensions.append("sphinx_tags")
tags_create_tags = True
# Whether to display tags using sphinx-design badges.
tags_create_badges = True
extensions += ["sphinxcontrib.youtube"]

language = "en"
html_last_updated_fmt = ""

todo_include_todos = True

html_use_index = True
html_domain_indices = True

copyright = f"2023-{now.year}, {author} Built with sphinx {sphinx.__version__} Python {platform.python_version()}"

rst_prolog = """
.. |jjr| image:: /images/jjr_avatar.png
.. |FluxWeb| image:: /images/rss_avatar.webp
"""
